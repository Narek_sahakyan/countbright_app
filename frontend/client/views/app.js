import React from 'react';

import Menu from '../components/menu';

// Global CSS styles
import './global.css';

const App = () => (
  <div className="app-container">
    <Menu />
    <div className="page-container"></div>
  </div>
);

export default App;