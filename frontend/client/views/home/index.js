import React from 'react';

export default class Home extends React.Component {

    constructor() {
        super();
        this.state = { name: "Test" };
        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler() {
        this.setState({ name: "test" });
    }

    render() {
        return (
            <h1 onClick = { this.clickHandler } > { `Hello ${this.state.name}!` } </h1>
        );
    }
}