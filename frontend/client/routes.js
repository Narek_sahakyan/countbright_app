import React from 'react';
import { Router, Route, Link } from 'react-router-dom'
import { createBrowserHistory } from 'history';


import App from './views/app';
import Home from './views/home';

const browserHistory = createBrowserHistory();

export default () => (
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <Route path='home' component={Home} />
    </Route>
  </Router>
);